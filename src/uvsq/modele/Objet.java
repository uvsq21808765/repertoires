package uvsq.modele;

public abstract class Objet {
	 private String nom;

	/**
	* Constructeur d'objets
	*/
	public Objet(String nom)
	{
		this.nom = new String(nom);
			
	}
	/**
	* obtenir le nom des objets
	* @return String nom
	*/	
	public String getNom() //ajout
	{
	       return nom;
	}
	/**
	* Définir le nom des objets
	* @param String nom
	*/
	public void setNom(String nom) //ajout
	{
	       this.nom= nom;
	}
	/**
	* obtenir la taille des objets
	* @return int taille
	*/	
	public abstract int getTaille ();

	public boolean isDirectory() {
		return "class uvsq.modele".equals(this.getClass().toString());
	}
}
