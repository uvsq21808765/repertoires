package uvsq.modele;

import uvsq.modele.Fichier;
import uvsq.modele.Repertoire;

public class Main {
	public static void main(String[] args) {
		// Build file hierarchy
		Repertoire root = new Repertoire("/");
		Repertoire usr = new Repertoire("/bin");
		root.ajouterObjet(usr);
		Repertoire home = new Repertoire("/home");
		root.ajouterObjet(home);
		Fichier home_tgz = new Fichier("home.tgz", 183);
		root.ajouterObjet(home_tgz);
		Repertoire thomas = new Repertoire("/home/thomas");
		home.ajouterObjet(thomas);
		Repertoire mohsen = new Repertoire("/home/mohsen");
		home.ajouterObjet(mohsen);
		Fichier thomasFichier01 = new Fichier("thomasFichier01", 5);
		thomas.ajouterObjet(thomasFichier01);
		Fichier thomasFichier02 = new Fichier("thomasFichier02", 10);
		thomas.ajouterObjet(thomasFichier02);
		Fichier mohsenFichier01 = new Fichier("mohsenFichier01", 50);
		mohsen.ajouterObjet(mohsenFichier01);
		Fichier mohsenFichier02 = new Fichier("mohsenFichier02", 100);
		mohsen.ajouterObjet(mohsenFichier02);
		//dfghjkfghjkl
		// Quelque taille
		System.out.println("size(/) = " + root.getTaille());
		System.out.println("size(/home) = " + home.getTaille());
		System.out.println("size(home.tgz) = " + home_tgz.getTaille());
		
		// Contient
		System.out.println(root + " Contient " + home + " ? " + root.existsWithSameName(home));
		System.out.println(root + " Contient " + mohsen + " ? " + root.existsWithSameName(mohsen));
		System.out.println(root + " Contient " + mohsenFichier01 + " ? " + root.existsWithSameName(mohsenFichier01));
		System.out.println(thomas + " Contient " + mohsen + " ? " + thomas.existsWithSameName(mohsen));
	
		//Ajout
		System.out.println("ajouter " + root + " dans " + thomas + " = " + thomas.ajouterObjet(root));
		System.out.println("ajouter " + thomas + " dans " + thomas + " = " + thomas.ajouterObjet(root));
		System.out.println("ajouter " + home_tgz + " dans " + thomas + " = " + thomas.ajouterObjet(home_tgz));
	}
}

