package uvsq.modele;

import uvsq.modele.Objet;

public class Fichier extends Objet{

    private int taille = 0;
    

    public Fichier(String nom, int taille){
    
       super(nom);  
       //contrôle sur la taille
		if(taille<0){
			throw new IllegalArgumentException(" impossible car la taille est négative");
		}
	  this.taille=taille;
    }
	
	/**
	*obtenir la taille du fichier
	*@return int taille
	**/
   @Override
    public int getTaille(){
    	return taille;
    }
}