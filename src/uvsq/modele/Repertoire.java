package uvsq.modele;

import java.util.ArrayList;
import java.util.Iterator;

/*import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;*/


public class Repertoire extends Objet{

	private ArrayList<Objet> arrList= new ArrayList<Objet>(); 
	
	
    /**
     * Constructeur d'objets de classe Répertoire
     */
    public Repertoire(String nom)
    {
        super(nom);
    }

    /**
     * Ajouter un objet au répertoire
     * 
     * @param l'objet à ajouter
     * @return  void
     */
    
  
    public boolean ajouterObjet(Objet objet) {
		boolean ret = false;
		if (objet != this) {
			if (!objet.isDirectory() || ((Repertoire) objet).existsWithSameName(this)) { 
				this.arrList.add(objet);
				ret = true;
			}
		}
		return ret;
	}
	

  /*public void ajouterObjet(Objet objet)
    {

		if(objet == null) throw new IllegalArgumentException("Vide");
		else if (this==objet) throw new IllegalArgumentException("Impossible d'ajouter");
		else if (existsWithSameName(objet)) throw new IllegalArgumentException("Changer le nom");
		else {
		arrList.add(objet);
		}
		
    }*/
   
	/**
	*Vérifiez si l'objet existe avec le même nom
	*@param l'objet à vérifier 
	*@return  true si il existe
	**/
	public boolean existsWithSameName(Objet objet){
	 Iterator<Objet> itr = arrList.iterator();
		while (itr.hasNext()) {
		Objet element = itr.next();
			if(element.getNom().equals(objet.getNom())) return true;
		}
		return false;
	}

    /**
	* Obtenir la taille d'un dossier
	*@param void 
	*@return  int taille
	**/
       @Override
    public int getTaille(){
    	int taille = 0;
    	
        Iterator<Objet> itr = arrList.iterator();
		while (itr.hasNext()) {
			Objet element = itr.next();
			taille += element.getTaille();
		}
		
		return taille;
    	
    }
   

}