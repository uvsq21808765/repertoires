package uvsq.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import uvsq.modele.Exceptions;
import uvsq.modele.Fichier;
import uvsq.modele.Repertoire;


public class TestRepertoire {
	/**
	*Test de la création du dossier
	*/
	@Test
	public void testCalculTaille() {
		Repertoire repertoire = new Repertoire ("rep1");
		assert repertoire.getTaille() >= 0;
	}
	
	/**
	*La taille du dossier doit être = 5
	*/
	@Test
	public void testContenant() throws Exceptions {
		
		Repertoire repertoire = new Repertoire ("rep1");
		Fichier f1 =  new Fichier("f1", 5);
		repertoire.ajouterObjet(f1);
		assertEquals(5, repertoire.getTaille());
	}
	
	/**
	*Test de la taille d'un dossier contenant un fichier dans un dossier
	*/
	@Test
	public void testdoubleContenant() throws Exceptions {
		
		Repertoire repertoire = new Repertoire ("rep1");
		Repertoire repertoire2 = new Repertoire ("rep2");
		Fichier f1 =  new Fichier("f1", 5);
		repertoire.ajouterObjet(f1);
		repertoire2.ajouterObjet(repertoire);
		assertEquals(5, repertoire2.getTaille());
	}
	
	/**
	*Test de la taille d'un dossier contenant deux fichiers
	*/
	@Test
	public void testdoubleFichier() throws Exceptions {
		
		Repertoire repertoire = new Repertoire ("rep1");
		
		Fichier f1 =  new Fichier("f1", 5);
		Fichier f2 =  new Fichier("f2", 20);
		repertoire.ajouterObjet(f1);
		repertoire.ajouterObjet(f2);
		assertEquals(25, repertoire.getTaille());
	}
	
	/**
	*Test de l'ajout d'un fichier null dans un dossier
	*/
	@Test(expected=IllegalArgumentException.class)
	public void testnullFichier() throws Exceptions {
		
		Repertoire repertoire = new Repertoire ("rep1");
		
		Fichier f1 =  null;
		
		repertoire.ajouterObjet(f1);
		
	}
	
	/**
	*Test de l'ajout de deux fichiers avec le même nom
	*/
	@Test(expected=IllegalArgumentException.class)
	public void testdoublenameFichier() throws Exceptions {
		
		Repertoire repertoire = new Repertoire ("rep1");
		
		Fichier f1 =  new Fichier("f1", 5);
		Fichier f2 =  new Fichier("f1", 20);
		repertoire.ajouterObjet(f1);
		repertoire.ajouterObjet(f2);
	}

	/**
	*Test de l'ajout d'un dossier dans un autre avec le même nom
	*/
	@Test(expected=IllegalArgumentException.class)
	public void testNonAjoute() throws Exceptions {
		
		Repertoire repertoire = new Repertoire ("rep1");
		
	
		repertoire.ajouterObjet(repertoire);
	}
}