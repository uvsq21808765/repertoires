package uvsq.test;

import org.junit.Assert;
import org.junit.Test;
import junit.framework.TestCase;
import uvsq.modele.Fichier;

public class TestFichier {
	/**
	*Test de la création du fichier
	*/
	@Test 
	public void testOK(){
		Fichier fichier = new Fichier (new String("test"), 10);
	}
	
	/**
	*Test de la création du fichier avec une taille négative
	*/
	@Test(expected=IllegalArgumentException.class)
	public void testCalculTaille() {
		Fichier fichier = new Fichier (new String("fic1"), -10);
	}
	
	/**
	*Test ajoute un fichier et teste sa taille
	*/
	@Test 
	public void testTaille(){
		int taille;
		Fichier fichier = new Fichier (new String("test"), 10);
		taille =fichier.getTaille();
		Assert.assertEquals(taille, 10);		
	}

 
}
