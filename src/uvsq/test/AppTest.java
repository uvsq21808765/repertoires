package uvsq.test;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest extends TestCase
{
/**
 * Créer le cas de test
 *
 * @param testName nom du cas de test
 */
public AppTest( String testName )
{
    super( testName );
}

/**
 * @return la suite de tests en cours de test
 */
public static Test suite()
{
    return new TestSuite( AppTest.class );
}

/**
 * Test rigoureux
 */
public void testApp()
{
    assertTrue( true );
}
}

